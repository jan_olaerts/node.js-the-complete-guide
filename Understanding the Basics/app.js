const http = require('http');
const routes = require('./routes');

// use the function stored in routes.js to handle incoming requests
const server = http.createServer(routes);

console.log(routes.someText);

server.listen(3000);